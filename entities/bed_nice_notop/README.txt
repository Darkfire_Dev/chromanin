Two options:

1) Place this folder into your redist/entities/furniture folder.

2) Create a folder inside redist/entities/ named my_models (or something similar) and place it in that folder. This option lets you avoid cluttering your custom models and Amnesia's, and makes it easy to put together your download file for your story/FC.

Enjoy!